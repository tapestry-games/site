export const state = () => ({
  current: '',
})

export const mutations = {
  set(state, scene) {
    state.current = scene
  },
}
