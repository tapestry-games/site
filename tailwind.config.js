module.exports = {
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      animation: {
        wiggle: 'wiggle 1s ease-in-out infinite',
      },
      keyframes: {
        wiggle: {
          '0%, 100%': { transform: 'rotate(-30deg)' },
          '50%': { transform: 'rotate(30deg)' },
        },
      },
      inset: {
        30: '7.5rem',
      },
      height: {
        '1/10s': '10vh',
        '2/10s': '20vh',
        '3/10s': '30vh',
        '4/10s': '40vh',
        '5/10s': '50vh',
        '6/10s': '60vh',
        '7/10s': '70vh',
        '8/10s': '80vh',
        '9/10s': '90vh',
      },
      maxHeight: {
        '90p': '90%',
        '95p': '95%',
      },
      width: {
        '2/10s': '20vw',
        '3/10s': '30vw',
        '1/10s': '10vw',
        '4/10s': '40vw',
        '5/10s': '50vw',
        '6/10s': '60vw',
        '7/10s': '70vw',
        '8/10s': '80vw',
        '9/10s': '90vw',
      },
      rotate: {
        '1/16': '22.5deg',
        '2/16': '45deg',
        '3/16': '67.5deg',
        '4/16': '90deg',
        '5/16': '112.5deg',
        '6/16': '135deg',
        '7/16': '157.5deg',
        '8/16': '180deg',
        '9/16': '202.5deg',
        '10/16': '225deg',
        '11/16': '247.5deg',
        '12/16': '270deg',
        '13/16': '292.5deg',
        '14/16': '315deg',
        '15/16': '337.5deg',
      },
      fontFamily: {
        code: ['Fira Code', 'Montserrat', 'Roboto'],
      },
    },
  },
  variants: {
    extend: {
      boxShadow: ['active'],
      scale: ['active', 'hover'],
    },
  },
  plugins: [require('@tailwindcss/typography')],
}
