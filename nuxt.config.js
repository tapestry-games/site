const isGitLab =
  process.env.NODE_ENV === 'production' && process.env.CI_PROJECT_NAME

export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',

  ssr: false,

  generate: {
    // fallback: true,
    exclude: [/^\/room/],
  },

  router: {
    mode: isGitLab ? 'hash' : 'history', // easy way to get the dynamic links to work on github pages
    base: isGitLab ? `/${process.env.CI_PROJECT_NAME}/` : '/',
  },

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Party Room',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: "Let's Party!!!",
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['@/assets/css/elements.css'],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    '@nuxtjs/tailwindcss',
    '@nuxtjs/fontawesome',
    // https://github.com/nuxt-community/google-fonts-module
    '@nuxtjs/google-fonts',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Content module configuration (https://go.nuxtjs.dev/config-content)
  content: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},

  fontawesome: {
    component: 'fa',
    suffix: false,
    icons: {
      solid: ['faDiceD20', 'faDiceD6', 'faScroll', 'faUsers'],
    },
  },

  googleFonts: {
    families: {
      'Fira Code': true,
      Montserrat: true,
      Roboto: true,
    },
  },
}
