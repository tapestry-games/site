export default {
  methods: {
    summarize(skillCheck) {
      return skillCheck.result
        .map((d) => d.value)
        .reduce((acc, value) => {
          if (acc[value]) {
            acc[value]++
          } else {
            acc[value] = 1
          }

          return acc
        }, {})
    },

    rollString(skillCheck) {
      const sidesCount = skillCheck.result
        .map((d) => d.sides)
        .reduce((acc, value) => {
          if (acc[value]) {
            acc[value]++
          } else {
            acc[value] = 1
          }

          return acc
        }, {})

      return Object.entries(sidesCount)
        .map((s) => `${s[1]}d${s[0]}`)
        .join(', ')
    },
  },
}
