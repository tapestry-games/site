export default {
  methods: {
    allD10s(skillCheck) {
      return skillCheck.result.filter((d) => d.sides !== 10).length === 0
    },

    hasOnes(skillCheck) {
      return skillCheck.result.filter((d) => d.value === 1).length > 0
    },

    exaltedValue(dieValue) {
      if (dieValue === 10) {
        return 2
      } else if (dieValue >= 7) {
        return 1
      } else {
        return 0
      }
    },

    exaltedSuccesses(skillCheck) {
      const successes = skillCheck.result
        .map((d) => this.exaltedValue(d.value))
        .reduce((acc, val) => acc + val, 0)

      return successes === 0 && this.hasOnes(skillCheck) ? -1 : successes
    },

    exaltedSummary(skillCheck) {
      const successes = this.exaltedSuccesses(skillCheck)

      if (successes === -1) {
        return 'Botch!!'
      } else if (successes === 0) {
        return 'Fail!'
      } else {
        return successes === 1 ? '1 success' : `${successes} successes`
      }
    },
  },
}
